'use strict'

const fs = require('fs')
const parse = require('./../parsers/nwk.js')
const rfd = require('./../metrics/rf.js')
const rfl = require('./../metrics/rfl.js')
const convertHrtime = require('convert-hrtime')
const path = require('path')

const treeType = 'unrooted'

const twoThousandTree = parse(fs.readFileSync(path.join(__dirname,'/resources/tree2000.nwk')).toString(),treeType)
const twoThousandTree1 = parse(fs.readFileSync(path.join(__dirname,'./resources/treeOne2000.nwk')).toString(),treeType)

const fourThousandTree = parse(fs.readFileSync(path.join(__dirname,'./resources/tree4000.nwk')).toString(),treeType)
const fourThousandTree1 = parse(fs.readFileSync(path.join(__dirname,'./resources/treeOne4000.nwk')).toString(),treeType)

const sixThousandTree = parse(fs.readFileSync(path.join(__dirname,'./resources/tree6000.nwk')).toString(),treeType)
const sixThousandTree1 = parse(fs.readFileSync(path.join(__dirname,'./resources/treeOne6000.nwk')).toString(),treeType)

const eightThousandTree = parse(fs.readFileSync(path.join(__dirname,'./resources/tree8000.nwk')).toString(),treeType)
const eightThousandTree1 = parse(fs.readFileSync(path.join(__dirname,'./resources/treeOne8000.nwk')).toString(),treeType)

const tenThousandTree = parse(fs.readFileSync(path.join(__dirname,'./resources/tree10000.nwk')).toString(),treeType)
const tenThousandTree1 = parse(fs.readFileSync(path.join(__dirname,'./resources/treeOne10000.nwk')).toString(),treeType)

/**
 * Measures the performance of the input function and returns the mean of it.
 * @param {Function} testFunc - Function that will be tested.
 * @param {Number} number - Number of times the function will be executed.
 * @param {String} label - Label that will be printed on stdout.
 * @return {Number} - Returns the average time in milleseconds of the function.
 */
function measurePerformance(testFunc,number){
    let sum = 0 //miliseconds
    for(let i = 0;i<number ; ++i){
        let t = process.hrtime()
        testFunc()
        t = process.hrtime(t)
        sum += convertHrtime(t).milliseconds
    }
    return sum/number //mean
}

function getClustersNodes(){
    console.log(measurePerformance(() => twoThousandTree.getClusters(), 1))
    console.log(measurePerformance(() => twoThousandTree1.getClusters(), 1))
    console.log(measurePerformance(() => fourThousandTree.getClusters(), 1))
    console.log(measurePerformance(() => fourThousandTree1.getClusters(), 1))
    console.log(measurePerformance(() => sixThousandTree.getClusters(), 1))
    console.log(measurePerformance(() => sixThousandTree1.getClusters(), 1))
    console.log(measurePerformance(() => eightThousandTree.getClusters(), 1))
    console.log(measurePerformance(() => eightThousandTree1.getClusters(), 1))
    console.log(measurePerformance(() => tenThousandTree.getClusters(), 1))
    console.log(measurePerformance(() => tenThousandTree1.getClusters(), 1))
    
}

function twoThousandNodesTree(){
    console.log(measurePerformance(() => rfd(twoThousandTree,twoThousandTree1),50))
    console.log(measurePerformance(() => rfl(twoThousandTree,twoThousandTree1),50))
}

function fourThousandNodesTree(){
    console.log(measurePerformance(() => rfd(fourThousandTree,fourThousandTree1),50))
    console.log(measurePerformance(() => rfl(fourThousandTree,fourThousandTree1),50))
}

function sixThousandNodesTree(){
    console.log(measurePerformance(() => rfd(sixThousandTree,sixThousandTree1),50))
    console.log(measurePerformance(() => rfl(sixThousandTree,sixThousandTree1),50))
}

function eightThousandNodesTree(){
    console.log(measurePerformance(() => rfd(eightThousandTree,eightThousandTree1),50))
    console.log(measurePerformance(() => rfl(eightThousandTree,eightThousandTree1),50))
}

function tenThousandNodesTree(){
    console.log(measurePerformance(() => rfd(tenThousandTree,tenThousandTree1),50))
    console.log(measurePerformance(() => rfl(tenThousandTree,tenThousandTree1),50))
}

module.exports.executeMeasures = function() {
    getClustersNodes()
    //twoThousandNodesTree()
    //fourThousandNodesTree()
    //sixThousandNodesTree()
    //eightThousandNodesTree()
    //tenThousandNodesTree()
}