'use strict'
const radixSort = require('./../sortingAlgorithms/radixSort.js')
const insertionSort = require('./../sortingAlgorithms/insertionSort.js')
const quickSort = require('./../sortingAlgorithms/quickSort.js')

const unSortedArray = ['CBA','AB','ABC','EDF','CFGH']
const sortedArray = ['AB','ABC','CBA','CFGH','EDF']

const sortedBigArray = ['AB','ABC','ACB','BA','BA','BAC','BCA','BCA','CBA','CFGH',
    'CFGH','DEF','EDF','FDC','GHFC','OLF','QLK','SFR','YYY','ZWS']

const unSortedBigArray = ['CBA','AB','ABC','EDF','CFGH','BAC','BA','BCA','DEF',
    'GHFC','ACB','BA','BCA','FDC','CFGH','SFR','ZWS','YYY','QLK','OLF']

module.exports.testRadix = function(test){
    let testArray = unSortedBigArray
    radixSort(testArray,t => t)
    let equals = compareArrays(testArray,sortedBigArray)
    test.equal(true,equals)
    test.done()
}

module.exports.testQuickSort = function(test){
    let testArray = unSortedArray
    quickSort(testArray,t => t)
    let equals = compareArrays(testArray,sortedArray)
    test.equal(true,equals)
    test.done()
}

module.exports.testInsertionSort = function (test) {
    let testArray = unSortedArray
    insertionSort(testArray,0,testArray.length-1,0,t=>t)
    let equals = compareArrays(testArray,sortedArray)
    test.equal(true,equals)
    test.done()
}

/**
 * Auxiliar function that compaes if two arrays are equals.
 * @param {Array} exp - expected array.
 * @param {Array} act - actual array.
 * @returns {boolean} - returns true if both array are equals and false if not.
 */
function compareArrays(exp,act){
   for(let i = 0;i < exp.length; ++i){
       if(exp[i] != act[i])
           return false;
    }
    return true;
}