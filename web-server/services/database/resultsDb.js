const mongoDb = require('./mongoDbService.js')

/**
 * Insert new document for a new job
 * 
 * @param {String} email - The email of the user
 * @returns  {*|UnorderedBulkOperation|Promise|OrderedBulkOperation}
 */
function createDocument(email){
    return mongoDb.insertDocument({email})
}

/**
 * Add the tree processing and the metric results to the document.
 *
 * @param {String} id - Id from the document to be updated.
 * @param {Object} trees - Tree processing to be added to the document.
 * @param {Object} results - Results to be added to the document.
 * @returns {Query|UnorderedBulkOperation|*|{filter, update}|Promise|OrderedBulkOperation}
 */
function addResults(id, trees, results, error){
    if(error != undefined)
        return mongoDb.updateDocument(id, {error})
    return mongoDb.updateDocument(id, {trees, results})
}

/**
 * Retrieves the document from the database.
 *
 * @param {String} id - Id of the document.
 * @returns {Promise} Promise with the document information if found.
 */
function getDocument(id){
    return mongoDb.getDocumentById(id)
    .then(result => {
        if(result.length == 0){
            const err = new Error('The id entered does not exist')
            err.status = 400
            return Promise.reject(err)
        }
        return result
    })
    .catch(err => Promise.reject(err))
}

/**
 * Removes document from database.
 * @param id
 * @return {Promise|Promise.<TResult>|*}
 */
function deleteDocument(id){
    return mongoDb.deleteDocument(id)
      .then(result =>{
          return result
      })
      .catch(err => Promise.reject(err))
}

module.exports = {
    createDocument,
    addResults,
    getDocument,
    deleteDocument
}