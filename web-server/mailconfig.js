'use strict'

const from = 'phyta.team@gmail.com'

let transporter = {
  service: 'gmail',
  auth: {
    user: from,
    pass: 'phytateam2017'
  }
}

module.exports = {
  transporter,
  from
}