'use strict'
const parse = require('./../../parsers/nwk.js')
const rfl = require('./../../metrics/rfl.js')

const rootedType = "rooted"
const unrootedType = "unrooted"

const rt = "(A,B,(C,D)E)F;"
const rt1 = "(C,B,(A,D)F)E;"

const pop = "(A:0.1,B:0.2,(C:0.3,D:0.4):0.5);"
const pop1 = "(B:0.1,A:0,(D:0.4,C:0.3):0.3);"

module.exports.testRobinsonFouldsLengthRootedSameTree = function (test) {
    const tree = parse(pop,rootedType)
    const res = rfl(tree,tree).result
    test.equals(0,res)

    const tree1 = parse(rt,rootedType)
    const res1 = rfl(tree1,tree1).result
    test.equals(0,res)

    test.done()
}

module.exports.testRobinsonFouldsLengthRooted = function (test) {

    const tree = parse(pop,rootedType)
    const tree1 = parse(pop1,rootedType)
    const res = rfl(tree,tree1).result
    test.equals(0.4,res)

    const tree3 = parse(rt,rootedType)
    const tree4 = parse(rt1,rootedType)
    const res1 = rfl(tree3,tree4).result
    test.equals(0,res1)

    test.done()
}

module.exports.testRobinsonFouldsLengthUnRooted = function (test) {

    const tree = parse(pop,unrootedType)
    const tree1 = parse(pop1,unrootedType)
    const res = rfl(tree,tree1).result
    test.equals(0.4,res)

    const tree3 = parse(rt,unrootedType)
    const tree4 = parse(rt1,unrootedType)
    const res1 = rfl(tree3,tree4).result
    test.equals(0,res1)

    test.done()
}