const mongoDb = require('mongodb')

const connection = require('mongoose').connection

/**
 * Inserts new document into database.
 *
 * @param {object} data - Resource to be inserted.
 * @returns {*|UnorderedBulkOperation|Promise|OrderedBulkOperation}
 */
function insertDocument(data){
    return connection.collection('documents').insert(data)
}

/**
 * Returns all the documents from database.
 *
 * @returns {Promise} - Promise with an array that contains all the documents.
 */
function getAllDocuments(){
    return connection.collection('documents').find({}).toArray()
}
/**
 * Gets document from database with given id.
 *
 * @param {object} id - The id from document.
 * @returns {Promise} - Promise that contains array with document if found.
 */
function getDocumentById(id){
    return connection
        .collection('documents')
        .find({'_id': new mongoDb.ObjectId(id)})
        .toArray()
}

/**
 * Updates document from database with given id.
 *
 * @param {object} id - Id from the document
 * @param {object} data - Data to be
 * @returns {Query|UnorderedBulkOperation|*|{filter, update}|Promise|OrderedBulkOperation}
 */
function updateDocument(id, data){
    return connection
        .collection('documents')
        .updateOne(
            {'_id': new mongoDb.ObjectId(id)},
            { $set: data }
        )
}

/**
 * Remove document by id.
 * @param id
 */
function deleteDocument(id){
  return connection
    .collection('documents')
    .remove({'_id': new mongoDb.ObjectId(id)})
}

module.exports = {
    insertDocument,
    getAllDocuments,
    updateDocument,
    getDocumentById,
    deleteDocument
}