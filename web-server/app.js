let express = require('express')
let path = require('path')
let logger = require('morgan')
let cookieParser = require('cookie-parser')
let bodyParser = require('body-parser')
let index = require('./routes/index')
let cors = require('cors')
let mongoose = require('mongoose')
let app = express()
app.use(cors())

const url = 'mongodb://localhost:27017/phyta-server'
const options = {server: {socketOptions: {keepAlive: 1}}}
mongoose.connect(url, options)

app.use(logger('dev'))
app.use(bodyParser.json({limit: '50mb'}))
app.use(bodyParser.urlencoded({limit: '50mb', extended: false}))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))

app.use(function (req, res, next) {
  res.setHeader('Content-Type', 'application/json')
  next()
})

app.use('/', index)

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    let err = new Error('Not Found')
    err.status = 404
    next(err)
})

// error handler
app.use(function(err, req, res, next) {
  res.locals.message = err.message
  res.locals.error = req.app.get('env') === 'development' ? err : {}

  res.status(err.status || 500)
  res.send({'error': err.message})
})

module.exports = app