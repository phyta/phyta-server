const resultsDb = require('./database/resultsDb.js')
const nodemailer = require('nodemailer')
const config = require('./../mailconfig.js')

/**
 * Queue used to store jobs for processing
 */
const jobQueue = [] 

/**
 * Configurations of the email
 */
const mailOptions = {
  from: config.from,
  subject: 'PhyTA results ready'
}

/**
 * Saves a job in the job queue and returns the id of the job to the client.
 * 
 * @param {String} parser - The parser to parse the trees.
 * @param {Array} trees -The trees for processing.
 * @param {Array} types - The types of the trees.
 * @param {Array} metrics - The metrics to compare the trees.
 * @param {String} email -The email of the user to notify when the results are ready.
 * @returns {Promise|Promise.<Number>} Returns the id for the processing job.
 */
function process(parser, trees, types, metrics, email){
    return resultsDb.createDocument(email)
    .then(result => {
        const id = result.insertedIds[0].valueOf()
        jobQueue.push({id, trees, metrics, types, parser})
        return {id}
    })
    .catch(err => Promise.reject('There was an error. Please try again later.'))
}

/**
 * Check for the results of a processing job.
 * 
 * @param {String} id -The id of the processing job.
 * @returns {Object} Returns the results for the processing job.
 */
function checkResults(id){
    return resultsDb.getDocument(id)
    .then(result => {
        if(result[0].results == undefined && result[0].error == undefined)
            return {'results': 'No results have been produced yet'}
        return result[0]
    })
    .catch(err => Promise.reject(err))
}

/**
 * Add the results of a processing job to the database and notify the user.
 * 
 * @param {Object} results - The results of a processing job. Contains the id of the job.
 */
function addResults(results){
  resultsDb.addResults(results._id, results.trees, results.results, results.error)
  .then(result => {
    resultsDb.getDocument(results._id)
    .then(doc => {
      const email = doc[0].email
      if(email){
        const msg = 'Your results with id: ' + results._id + ' have been processed and are ready to be analyzed.'
        sendMail(email, msg)
      }
    })
  })
  .catch(err => {
    resultsDb.getDocument(results._id)
    .then(doc => {
      const email = doc[0].email
      if(email){
        const msg = 'There was an error processing the job with id: ' + results._id + '.'
        sendMail(email, msg)
      }
    })
  })
}

/**
 * Sends a mail.
 * 
 * @param {String} to - The email of the user to send the email.
 * @param {String} text - The content of the email.
 */
function sendMail(to, text){
  mailOptions.to = to
  mailOptions.text = text
  nodemailer.createTransport(config.transporter).sendMail(mailOptions)
  .then(info => console.log(info.response))
  .catch(err => console.log(err))
}

/**
 * Deletes job from database.
 * @param jobId
 * @return {Promise|Promise.<TResult>|*}
 */
function deleteResults(jobId) {
  return resultsDb.deleteDocument(jobId)
}

module.exports = {
  process,
  checkResults,
  addResults,
  deleteResults,
  jobQueue
}