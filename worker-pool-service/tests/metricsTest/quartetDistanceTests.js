'use strict'
const quart = require('./../../metrics/quart.js')
const parse = require('./../../parsers/nwk.js')

const type = 'unrooted'
const pop = '((E,D)G,(C,(B,A)I)H)F;',pop1 = '((E,(D,C)I)G,(B,A)H)F;',pop2 = '(C,(A,B)G)F;'

module.exports.testQuartetSameTree = function(test){
  const tree = parse(pop,type)
  const res = trip(tree,tree).result
  test.equals(0,res)
  test.done()
}

module.exports.testQuartetDifTree = function(test){
  const tree = parse(pop,type),tree1 = parse(pop1,type)
  const res = trip(tree,tree1).result
  test.equals(5,res)
  test.done()
}

module.exports.testQuartetWithException = function (test) {
  const tree = parse(pop2,type),tree1 = parse(pop1,type)
  test.equals('Trees must have the same number of leaves!',quart(tree,tree1))
  test.done()
}