let express = require('express')
let router = express.Router()
const service = require('./../services/processService.js')
const token = 'D1ICgC4BzJSPjO4GHWHm8zY6eJceC9Vm'
const path = require('path')

const re = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i
  
router.post('/process', (req, res, next) => {
  
  let err
  if(req.body.trees == undefined){
    err = new Error('Please provide tree data!')
    err.status = 400
    return next(err)
  }
  if(req.body.treeTypes == undefined){
    err = new Error('Please provide tree types data!')
    err.status = 400
    return next(err)
  }
  if(req.body.metrics == undefined){
    err = new Error('Please provide metrics to compare data!')
    err.status = 400
    return next(err)
  }
  if(req.body.parser == undefined){
    err = new Error('Please provide the parser to parse the data!')
    err.status = 400
    return next(err)
  }
  
  const trees = req.body.trees.split('$')
  const types = req.body.treeTypes.split('$')

  if(trees.length < 2){
    err = new Error('Please provide at least two trees!')
    err.status = 400
    return next(err)
  }

  if(trees.length != types.length){
    err = new Error('Please provide the same number of trees and tree types!')
    err.status = 400
    return next(err)
  }

  if(req.body.metrics.length < 1){
    err = new Error('Please provide at least one metric!')
    err.status = 400
    return next(err)
  }

  const email = req.body.email
  if(email != undefined && !re.test(email)){
    err = new Error('Please provide a valid email!')
    err.status = 400
    return next(err)
  }
  
  const metrics = req.body.metrics.split('$')

  service.process(req.body.parser, trees, types, metrics, email)
    .then(response => {
      res.send({'id': response.id})
    })
    .catch(err => next(new Error(err)))
})

router.get('/checkResults/:id', (req, res, next) => {
  service.checkResults(req.params.id)
  .then(results => {
    delete results.email
    res.send(results)
  })
  .catch(err => {
      if(err.status == 400) return next(err)
      return next(new Error('There was an error. Please try again later.'))
    })
})

router.get('/configFile', (req, res, next) => {
  let root = path.join(__dirname+'/../../','config.json')
  res.sendFile(root)
})

router.post('/sendResults',isCron, (req, res, next) => {
  const results = JSON.parse(req.body.results)
  service.addResults(results)
  res.send({ok: 'DONE'})
})

router.get('/getJob',isCron, (req, res, next) => {
  let job = service.jobQueue.shift()
  if(job == undefined) job = {}
  res.send(JSON.stringify(job))
})

router.delete('/deletejob/:jobId',(req, res, next)=>{
  const jobId = req.params.jobId
  service.deleteResults(jobId)
    .then(result => {
      res.send({ok: 'Deleted job'})
    })
    .catch(err => {
      next(err)
    })
})

function isCron(req,res,next) {
  if(req.query.token === token){
    return next()
  }

  if(JSON.parse(req.body.results).token === token){
    return next()
  }

  const err = new Error('forbidden')
  err.status = 403
  return next(err)
}

module.exports = router