'use strict'
const nwk = require('./../parsers/nwk')
const Tree = require('./../model/tree.js')

const rooted = 'rooted'
const unrooted = 'unrooted'
const Normaltree = '(A:1,B:2,(C:3,D:4)E:5)F;'
const badTree = '(A:1,B:2,(C:3,D~4)E:5)F;'

module.exports.normalTreeRootedFormat = function(test){
    const tree = nwk(Normaltree,rooted)
    test.ok(tree instanceof Tree)

    const unrootedTree = nwk(Normaltree,unrooted)
    test.ok(unrootedTree instanceof Tree)

    test.expect(2)
    test.done()
}

module.exports.badTreeFormat = function (test) {
    let error = false
    try{
        nwk(badTree,rooted)
    }catch(err){
        error = true
    }

    test.ok(error,'Error occured in parser')
    test.expect(1)
    test.done()
}